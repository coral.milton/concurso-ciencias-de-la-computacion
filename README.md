# Analysis and Representation of Data from the Ministry of Health in Ecuador Using the 7 Most Common Clinical Data Points



## PROPOSAL DESCRIPTION

This project aims to perform an analysis using heatmaps and a dataset provided by the MSP (Ministry of Public Health). This dataset contains a total of 1,048,575 outpatient consultation records collected during 2021. The dataset includes information such as: Province, Zone, Date, Clinical Case Code, Description, etc. From these parameters, we will focus on the province, clinical case code, and description, as we aim to highlight the recurrence of these cases in each province by representing the data count in a heatmap. The goal is to identify which provinces are more prone and vulnerable to specific clinical cases and, based on this, to take the necessary measures.

## Dataset Description

The original dataset provided by the MSP consisted of more than 1 million records. From this dataset, we selected the most important variables and saved them after the first data curation. In the first curation, we identified the 7 most recurrent clinical cases, and then performed a second curation. In the second curation, we performed a count by grouping the data based on provinces and saved this information in the "Final Count Data."

# Process Description for Visualization

We used a mapping tool based on geographical locations by province. Each province was assigned a count, which was later used in Highcharts to generate the map. We then used Shiny, which allowed us to create the application, and with Shiny APP, we published our application.

#Application Link
https://mi1w0i-lancelot.shinyapps.io/APP_CASOS_CLINICOS_SHINY/



